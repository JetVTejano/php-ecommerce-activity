    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">Pokemon Breeders</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
            <?php
                if(isset($_SESSION['user'])){
            ?>
                <li class="nav-item active">
                    <a class="nav-link" href="../profile.php">Hi <?php echo $_SESSION['user']['firstName']?></a>
                </li>
                <?php
                        if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] === "1"){
                ?>
                <li class="nav-item active">
                    <a class="nav-link" href="../add-item.php">Add Item</a>
                </li>
                <li class="nav-item active">
                <li class="nav-item active">
                    <a class="nav-link" href="../all-orders.php">All Orders</a>
                </li>
                <?php
                }else{
                ?>
                <li class="nav-item active">
                <li class="nav-item active">
                    <a class="nav-link" href="../order-history.php">Order History</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../cart.php">Cart <span class="badge badge-info" id="cartCount"><?php
                    if(isset($_SESSION['cart'])){
                        //We need to get the sum of all the quantity in our session-cart.
                        echo array_sum($_SESSION['cart']);
                    }else{
                        echo "0";
                    }
                    ?></span></a>
                </li>
                <?php
                }
                ?>
                <li class="nav-item active">
                    <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
                </li>
            <?php
                }else{

            ?>
                <li class="nav-item active">
                    <a class="nav-link" href="../login.php">Login</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../register.php">Register</a>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
    </nav>