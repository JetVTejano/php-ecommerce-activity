<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Bootswatch -->
    <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.css">

    <!-- This is our jQuery -->
    <script defer src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <!-- If a file is a dependency of another file, it should be at the top -->
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Registration Validation Script -->
    <script src="../assets/scripts/register.js" defer></script>

    <!-- Add to Cart JS -->
    <script src="../assets/scripts/addToCart.js" defer></script>
    
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    
    <title><?php getTitle(); ?></title>
</head>
<body>
<?php require "navbar.php";?>
    <!-- This is where we want to insert the content of the pages
    We will create a function in every page, to get the content of that page -->
    <?php
        getContent();
    ?>
<?php require "footer.php";?>
</body>
</html>