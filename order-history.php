<!-- Activity:

Publish the order data of the currently logged in user.

In this table, you need to publish the order details:

Columns:
Order Id - Order Date - Items with Quantity - Total - Status - Payment Method -- Cancel Button

Challenge 1 - Publish Items with Quantity from item_order using JOIN
Challenge 2 - Instead of publishing status_id as a number, publish it as the actual status
Challenge 3 - Same as challenge 2, but with payment_id

The cancel button is just a button, no need to make it work for now.-->

<?php
    require "template/template.php";

    function getTitle(){
        echo "Pokemon Breeders | Order History";
    };

    function getContent(){
        // We use require when we need to interact with data from the database.
        require "controllers/connection.php";

        // First line, is to choose the object from the Session.
        // Second line is to write the query that we need.
        // Third line applies the query to the database. (Still need to understand difference of mysqli_query and mysqli_fetch_assoc)

        $userId = $_SESSION['user']['id'];
        $order_query = "SELECT item_order.quantity, items.name FROM item_order JOIN items ON (item_order.item_id = items.id)";

        $order_query_result = mysqli_query($conn, $order_query);

    ?>    
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <h1 class="text-center py-3">Your Order History</h1>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Order ID:</th>
                                <th>Order Date:</th>
                                <th>Items and Quantity:</th>
                                <th>Total:</th>
                                <th>Status:</th>
                                <th>Payment Method:</th>
                                <th>Cancel Button:</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        // We need to get the order id, order date, total, status_id -> name, payment_id -> name.
                        // Notice that we skipped order details where we will publish the item name and the quantity.
                            $user_id = $_SESSION['user']['id'];
                        // In my orders query, we joined orders, statuses and payments.
                            $orders_query = "SELECT statuses.name AS status, payments.name AS payment, total, orderDate, orders.id AS order_id FROM orders JOIN statuses ON (statuses.id = orders.status_id) JOIN payments ON (payments.id = orders.payment_id) WHERE user_id = $user_id";
                            
                            //this query will result to an array of orders

                            $orders = mysqli_query($conn, $orders_query);
                            foreach($orders as $indiv_order){
                        ?>
                            <tr>
                                <td><?php echo $indiv_order['order_id']; ?></td>
                                <td><?php echo $indiv_order['orderDate']; ?></td>
                                <td>
                                    <?php 
                                    //this came from our first query
                                        $order_id = $indiv_order['order_id'];
                                    //to get the order details (item name and quantity), we joined items and item_order.
                                        $items_query = "SELECT items.name as item_name, item_order.quantity as quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $order_id";
                                        // this will result to an array of items

                                        $items = mysqli_query($conn, $items_query);
                                    
                                        foreach($items as $indiv_item){
                                    ?>
                                        <span><?php echo $indiv_item['quantity'] . " - " . $indiv_item['item_name'] ?></span><br>
                                    <?php
                                        }
                                    ?>
                                </td>
                                <td><?php echo $indiv_order['total']; ?></td>
                                <td><?php echo $indiv_order['status']; ?></td>
                                <td><?php echo $indiv_order['payment']; ?></td>
                                <td>                               
                                <?php
                                    if($indiv_order['status'] !== "paid"){
                                ?>
                                    <a href="controllers/process_cancel_order.php?order_id=<?php echo $indiv_order['order_id'];?>" class="btn btn-danger">Cancel</a>
                                <?php
                                    }
                                ?></td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>

    <?php
    
    }
?>