CREATE DATABASE b67Ecommerce;

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE roles(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE statuses(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE payments(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255),
	role_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(role_id)
	REFERENCES roles(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE items(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	description TEXT,
	price INT,
	quantity INT,
	category_id INT,
	imgPath VARCHAR(255),
	PRIMARY KEY (id),
	FOREIGN KEY(category_id)
	REFERENCES categories(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE orders(
	id INT NOT NULL AUTO_INCREMENT,
	total INT,
	orderDate DATE DEFAULT NOW(),
	status_id INT,
	payment_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY(status_id)
	REFERENCES statuses(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY (payment_id)
	REFERENCES payments(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE item_order(
	id INT NOT NULL AUTO_INCREMENT,
	item_id INT,
	order_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY(item_id)
	REFERENCES items(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY (order_id)
	REFERENCES orders(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

INSERT INTO roles(name) VALUES("admin"), ("customer");

INSERT INTO statuses(name) VALUES("pending"), ("paid"), ("cancelled");

INSERT INTO payments(name) VALUES("cash"), ("deposit"), ("paypal");

INSERT INTO categories(name) VALUES("shinies"), ("mythicals"), ("competitive");

INSERT INTO items(name, description, price, quantity, category_id, imgPath) VALUES
("Shiny Bulbasaur", "A shiny version of the grass-type Kanto Starter", 1000, 1, 1, "randomString"),
("Shiny Charmander", "A shiny version of the fire-type Kanto Starter", 1000, 1, 1, "randomString");

CREATE TABLE profiles(
	id INT NOT NULL AUTO_INCREMENT,
	address VARCHAR(255),
	contactNo VARCHAR(255),
	profileImg VARCHAR(255),
	user_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY(user_id)
	REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

ALTER TABLE orders ADD user_id INT;

ALTER TABLE orders ADD FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict;

-- Optional:
-- ALTER TABLE orders ALTER COLUMN orderDate DATE default now();

ALTER TABLE item_order ADD quantity INT;