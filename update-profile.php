<?php
    require "template/template.php";

    function getTitle(){
        echo "Pokemon Breeders | Update Profile";
    }

    function getContent(){

        require "controllers/connection.php";
        $user_id = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles where user_id = $user_id";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
?>
    <div class="d-flex justify content-center align-items-center flex-column">
        <h1 class="my-5">Update Profile</h1>
        <form action="controllers/process_update_profile.php" method="POST" enctype="multipart/form-data">

            <div class="form=group">
                <label for="firstName">First Name:</label>
                <input type="text" class="form-control" name="firstName"
                value="<?php echo $_SESSION['user']['firstName'];?>">
            </div>
            
            <div class="form=group">
                <label for="lastName">Last Name:</label>
                <input type="text" class="form-control" name="lastName"
                value="<?php echo $_SESSION['user']['lastName'];?>">
            </div>

            <div class="form=group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email"
                value="<?php echo $_SESSION['user']['email'];?>">
            </div>

            <div class="form=group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" name="password"
                value="<?php echo $_SESSION['user']['password'];?>">
            </div>

            <div class="form=group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address"
                value="<?php echo $profile['address']?>">
            </div>

            <div class="form=group">
                <label for="contactNo">Contact Number:</label>
                <input type="text" class="form-control" name="contactNo"
                value="<?php echo $profile['contactNo']?>">
            </div>

            <div class="form=group">
                <label for="profileImg">Upload Profile Image:</label>
                <!-- For a thumbnail for the user. -->
                <img src="<?php echo $profile['profileImg'] ?>" height="50px" class="rounded-circle" alt="">
                <input type="file" class="form-control" name="profileImg">
            </div>

            <div class="text-center py-2">
                <button type="submit" class="btn btn-success"> Update Profile
                </button>
            </div>
            
        </form>
    </div>
<?php
    }
?>