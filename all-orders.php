<!-- The cancel button is just a button, no need to make it work for now.-->

<?php
    require "template/template.php";

    function getTitle(){
        echo "Pokemon Breeders | All Orders";
    };

    function getContent(){
        require "controllers/connection.php";
    ?>    
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <h1 class="text-center py-3">List of All Orders</h1>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Order ID:</th>
                                <th>User ID:</th>
                                <th>User Name: </th>
                                <th>Order Date:</th>
                                <th>Items and Quantity:</th>
                                <th>Total:</th>
                                <th>Status:</th>
                                <th>Payment Method:</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 

                        // Step 1 is not required because we don't need a condition.

                        
                        // We need to get the order id, user ID, User name, order date, total, status_id -> name, payment_id -> name.
                        // Notice that we skipped order details where we will publish the item name and the quantity.
                        // In the orders query, lets join orders, statuses payments, and users.
                        // Step 2 of the process:
                            $all_orders_query = "SELECT orders.id AS order_id, users.id AS user_id, users.firstName AS username, orderDate, total, statuses.name AS status, payments.name AS payment FROM orders JOIN statuses ON (statuses.id = orders.status_id) JOIN payments ON (payments.id = orders.payment_id) JOIN users ON (users.id = orders.user_id) ORDER BY orders.id";
                            
                            //this query will result to an array of orders
                            //Step 3 of the process:
                            $all_orders = mysqli_query($conn, $all_orders_query);

                            foreach($all_orders as $indiv_order){
                        ?>
                            <tr>
                                <td><?php echo $indiv_order['order_id']; ?></td>
                                <td><?php echo $indiv_order['user_id']; ?></td>
                                <td><?php echo $indiv_order['username']; ?></td>
                                <td><?php echo $indiv_order['orderDate']; ?></td>
                                <td>
                                    <?php 
                                    //First Step of Three Step Process again:
                                        $order_id = $indiv_order['order_id'];
                                    //to get the order details (item name and quantity), we joined items and item_order. we used the first step in the WHERE portion of the query.
                                        $items_query = "SELECT items.name as item_name, item_order.quantity as quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $order_id";
                                        // this will result to an array of items
                                        $items = mysqli_query($conn, $items_query);
                                    
                                        foreach($items as $indiv_item){
                                    ?>
                                        <span><?php echo $indiv_item['quantity'] . " - " . $indiv_item['item_name'] ?></span><br>
                                    <?php
                                        }
                                    ?>
                                </td>
                                <td><?php echo $indiv_order['total']; ?></td>
                                <td><?php echo $indiv_order['status']; ?></td>
                                <td><?php echo $indiv_order['payment']; ?></td>
                                <td>
                                <!-- Cancel Button -->
                                <?php
                                    if($indiv_order['status'] !== "paid"){
                                ?>
                                    <a href="controllers/process_cancel_order.php?order_id=<?php echo $indiv_order['order_id'];?>" class="btn btn-danger">Cancel</a>
                                <?php
                                    }
                                ?>


                                <!-- Mark as Delivered Button -->
                                <?php 
                                    if($indiv_order['status'] !== "cancelled"){
                                ?>
                                    <a href="controllers/process_mark_as_delivered.php?order_id=<?php echo $indiv_order['order_id']; ?>" class="btn btn-success">Mark as Delivered</a>
                                <?php
                                    }
                                ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>

    <?php
    
    }
?>