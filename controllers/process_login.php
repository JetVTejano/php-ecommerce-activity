<?php
    require "connection.php";
    session_start();
    //Capture data from the form
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    // Create a query searching for an entry with the captured email
    $user_query = "SELECT * FROM users where email = '$email'";
    $user = mysqli_query($conn, $user_query);
    $user_info = mysqli_fetch_assoc($user);

    if(mysqli_num_rows($user) === 1){
        // Check the Password from the input if equal to the password in the database.
        // But remember, the password in the database is hashed.
        if(password_verify($password, $user_info['password'])){
           $_SESSION['user'] = $user_info;
           header("Location: ../index.php");
        } else{
            die("login failed, wrong password");
        }


    }else{
        die("Login Failed, Wrong email.");
    }

?>