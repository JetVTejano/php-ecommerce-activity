<?php
    $host = "localhost"; //This is our current host.
    $db_username = "root"; // This is the username of our host/
    $db_password = ""; // Password for host
    $db_name = "b67Ecommerce"; //Name of our database.

    //Create the connection
    $conn = mysqli_connect($host, $db_username, $db_password, $db_name);

    //Check if the connection is successful
    if(!$conn){
        die("Connection failed: " . mysqli_error($conn));
    }
?>