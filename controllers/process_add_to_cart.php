<?php
    session_start();

    //Capture the data that we need: Id of the item, quantity of the item.
    $item_id = $_POST['item_id'];
    $quantity_from_form = $_POST['quantity'];
    $quantity_from_db = $_POST['quantity_from_db'];

    //We need to check if the quantity added to cart is less than or equal to the actual quantity in our database. If not enough, error, if enough, proceed.
    if($quantity_from_form > $quantity_from_db){
        die('Insufficient');
    }else{
    
            //Save the item in our session.
            //If this is the first time to add an item, create the session and assign the quantity received to the session variable.
        if(isset($_SESSION['cart'][$item_id])){
            //We have to check if the total of the qty in the session is not more than the quantity in our db.
            if($_SESSION['cart'][$item_id] + $quantity_from_form > $quantity_from_db){
                die("Insufficient stocks.");
            }else{
            $_SESSION['cart'][$item_id] += $quantity_from_form;
            }
        }else{
            //If not, we only need to add the quantity to the existing quantity in our session variable.
            $_SESSION['cart'][$item_id] = $quantity_from_form;
        }
    }    
?>
