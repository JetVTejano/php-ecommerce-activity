<?php
// Since we are editing we also need to get the id of the item we are editing.
    require "connection.php";
    $itemId = $_POST['item_id'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $description = $_POST['description'];
    $category_id = $_POST['category_id'];


    $image_query = "SELECT imgPath FROM items WHERE id = $itemId";
    $image_result = mysqli_fetch_assoc (mysqli_query($conn, $image_query));

    // Check if the user uploaded a new image.
    // If not, save the old value of the image from the image_result.
    // If yes, process to saving the new uploaded image.
    $imgPath = "";
    if($_FILES['imgPath']['name']===""){
        $imgPath = $image_result['imgPath'];
    }else{
    // To save the uploaded image
    // Set up the destination of the image
    $destination = "../assets/images/";
    // Create the file name
    $fileName = $_FILES['imgPath']['name'];
    // Move the image
    move_uploaded_file($_FILES['imgPath']['tmp_name'], $destination.$fileName);
    // Create the imgPath variable that we will save in our database.
    $imgPath = $destination.$fileName;
    }

    //Assignment / Practice
    // Create the update query.
    $edit_item_query = "UPDATE items SET name = '$name', description = '$description', price = $price, quantity = $quantity, category_id = $category_id, imgPath = '$imgPath' WHERE id = $itemId";

    $edited_item = mysqli_query($conn, $edit_item_query);
    
    // Redirect back to Catalog
    //Redirect to Catalog page:
    header("Location: ../index.php");
 
?>