<!-- 1. Upon check our orders table, we need to insert the following data: 
a. total - we'll compute this
b. status_id - upon checking the statuses table, we'll use 1(pending)
c. payment_id - upon checking the payments table, we'll use 1(cash)
d. user_id - we can get this from the session;

2. But we noticed, we also have an item_order table; This table has the list of items in an order.

3. So, before we can update our item_order, we need to save first the order to get the order_id

4. But we also need to save the individual item_id of the order. We can get the item_ids from the SESSION['cart']
-->

<?php
    session_start();

    require "connection.php";

    // Initialize the orders data.
    $total = 0; //We'll recompute this later.
    $status_id = 1; //Pending
    $payment_id = 1; //Cash
    $user_id = $_SESSION['user']['id']; // Logged-in user.

    //Save it to our orders table:
    $order_query = "INSERT INTO orders (total, status_id, payment_id, user_id) VALUES ($total, $status_id, $payment_id, $user_id)";
    
    // Don't worry about the total, before we end this process we will update the total

    $result = mysqli_query($conn, $order_query);
    
    //Remember, we need to get the id of the order we just saved. (To put it into item_order table)
    //We will use a function which gets the last id that was inserted into the database. (In this case, we will get the id of $result)
    $order_id = mysqli_insert_id($conn);

    // We'll get the item_id from the session. AND AT THE SAME TIME, we will compute for the total
    //REMEMBER WHEN FOR EACHING AN ASSOC ARRAY
    foreach($_SESSION['cart'] as $item_id => $quantity){
        // But in order for us to compute the total, we also need to know the price of the item.
        $item_query = "SELECT price FROM items WHERE id = $item_id";

        // This will result in an an associative array named price with a property called price.
        $price = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
        
        // To access the actual value, we can use this:
        $subtotal = $price['price'] * $quantity;

        $total += $subtotal; //This updates the total amount.

        // This saves the different info we just collected into our database
        $item_order_query = "INSERT INTO item_order (item_id, order_id, quantity) VALUES ($item_id, $order_id, $quantity)";
        $result = mysqli_query ($conn, $item_order_query);
    };

    // We need to update the total of the order:

    $update_total = "UPDATE orders SET total = $total WHERE id = $order_id";
    $update_result = mysqli_query($conn, $update_total);

    // Remove the session or the items in cart and start fresh
    unset($_SESSION['cart']);

    header("Location: ../order-history.php");
?>