<?php
    //The goal is to remove the contents of the variable $_SESSION['cart']
    session_start();

    //To reset the value of the variable to null:
    unset($_SESSION['cart']);

    header("Location: " . $_SERVER['HTTP_REFERER']);
?>