<?php
    session_start();
// We need to remove all session variables
    session_unset();
// We need to destroy the session
    session_destroy();
// We need to redirect to catalog.
    header("Location: ../index.php");
?>