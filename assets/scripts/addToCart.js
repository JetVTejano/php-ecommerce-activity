// We need to get all add to cart btns.
const addToCartBtns = document.querySelectorAll('.addToCart')

// We need to add an event listener to each of the btns.

addToCartBtns.forEach(function(indivAddToCartBtn){
    indivAddToCartBtn.addEventListener('click', function(indivBtn){
        // Get the data from the btn.
        const quantity = indivBtn.target.parentElement.firstElementChild.value;
        const itemId = indivBtn.target.parentElement.firstElementChild.nextElementSibling.value;
        const quantityFromDb = indivBtn.target.previousElementSibling.previousElementSibling.value;
        const itemName = indivBtn.target.previousElementSibling.value;

        // Validate the qty.
        if(quantity <= 0){
            toastr['error']("Please input a correct quantity.")
        }else if (parseInt(quantity) > parseInt(quantityFromDb)){
            toastr['error']("Insufficient Stocks")
        }else{

            // If qty is correct, send data via fetch.
            const data = new FormData;

            data.append('item_id', itemId);
            data.append('quantity', quantity);
            data.append('quantity_from_db', quantityFromDb);

            fetch('../../controllers/process_add_to_cart.php', {
                method: "POST",
                body: data
            }).then(function(response){
                return response.text();
            }).then(function(response_from_fetch){
                if(response_from_fetch === "Insufficient"){
                    toastr['error']("Not enough stocks")
                }else{
                    toastr['success'](`Successfully added ${quantity} ${itemName} to cart.`)
                }
            })
        }
    });
})


