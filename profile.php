<?php
    require "template/template.php";

    function getTitle(){
        echo "Pokemon Breeders | Add Profile";
    };

    function getContent(){
        // We use require when we need to interact with data from the database.
        require "controllers/connection.php";

        // First line, is to choose the object from the Session.
        // Second line is to write the query that we need.
        // Third line applies the query to the database. (Still need to understand difference of mysqli_query and mysqli_fetch_assoc)
        $user_id = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles WHERE user_id = $user_id";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));

        if(!$profile){
    ?>
        <div class="d-flex justify-content-center align-items-center vh-100 flex-column">
        <h1 class="text-center py-5">You haven't updated your profile yet.</h1>
        <a href="add-profile.php" class="btn btn-info">Add your profile.</a>
        </div>
    
    <?php
        }else{
    ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <h1 class="text-center py-3">Your Profile</h1>
                        <div class="text-center py-3">
                        <!-- The echo tag here will get the data from the  profiles table in our database-->
                            <img src="<?php echo $profile['profileImg'];?>" height = "200px" class="rounded-circle">
                        </div>
                    <table class="table table-striped">

                        <!-- Stretch Goals: To get the First Name, Last Name, and Address, get the data from the session using the same way that you accessed the user IDs. Print them using echo into the table. -->
                        <tr>
                            <td>First Name:</td>
                            <td><?php echo $_SESSION['user']['firstName']; ?></td>
                        </tr>
                        <tr>
                            <td>Last Name:</td>
                            <td><?php echo $_SESSION['user']['lastName']; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><?php echo $_SESSION['user']['email']; ?></td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td><?php echo $profile['address']; ?></td>
                        </tr>
                        <tr>
                            <td>Contact Number:</td>
                            <td><?php echo $profile['contactNo']; ?></td>
                        </tr>
                    </table>

                    <div class="text-center">
                        <a href="update-profile.php" class="btn btn-info">Update Profile</a>
                        
                        <!-- Stretch Goal: Create the Delete Profile -->
                        <!-- <a href="controllers/process_delete_profile.php?profile_id=<?php // echo $profile['id']; ?>" class="btn btn-danger">Remove Profile</a> -->
                    </div>

                </div>
            </div>
        </div>

    <?php
        }
    }
?>