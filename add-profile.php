<?php
    require "template/template.php";

    function getTitle(){
        echo "Pokemon Breeders | Add Profile";
    };

    function getContent(){
?>

    <!-- Add Form Content -->
    <h1 class = "text-center py-5">Add Profile Details</h1>
    <div class = "d-flex justify-content-center align-items-center">

        <form action="controllers/process_add_profile.php" method="POST" class="mb-5" enctype="multipart/form-data">
        
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" name="address" class="form-control" placeholder="House Number. Street. Barangay. City.">
            </div>
            
            <div class="form-group">
                <label for="contact">Contact Number:</label>
                <input type="text" name="contact" class="form-control" placeholder="+63 9XX XXX XXXX">
            </div>
            
            <!-- For the image -->
            <div class="form-group">
                <label for="profileImgPath">Upload Profile Image:</label>
                <input type="file" name="profileImgPath" class="form-control">
            </div>
            
            <button class="btn btn-info" type="submit">
                Add Profile
            </button>

        </form>
    </div>

<?php
    }
?>